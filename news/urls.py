"""news URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.sitemaps.views import sitemap
from django.urls import path

from advertapp.views import advert_view, advert_edit
from newsapp.views import news_view, news_short, news_detail, news_short_list, PostSitemap, calendar
from photoapp.views import photo_view, photo_edit
from postapp.models import PostFeed, YandexRss, YandexDzenRss, YandexTurboRss
from postapp.views import post_view, post_edit, charter_view, charter_edit, user_view, user_password, \
    user_edit, robots, company_view, company_edit, post_content

sitemaps = {
    'static': PostSitemap,
}

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', news_view, name='news_view'),
    path('login', LoginView.as_view(next_page='post/'), name='login'),
    path('logout', LogoutView.as_view(next_page='/'), name='logout'),

    path('calendar/', calendar, name='calendar'),
    path('photo/', photo_view, name='photo_view'),
    path('photo/add/', photo_edit, name='photo_add'),
    path('photo/<pk>/edit/', photo_edit, name='photo_edit'),

    path('post/', post_view, name='post_view'),
    path('post/add/', post_edit, name='post_add'),
    path('post/<pk>/edit/', post_edit, name='post_edit'),

    path('charter/', charter_view, name='charter_view'),
    path('charter/add/', charter_edit, name='charter_add'),
    path('charter/<pk>/edit/', charter_edit, name='charter_edit'),

    path('advert/', advert_view, name='advert_view'),
    path('advert/add/', advert_edit, name='advert_add'),
    path('advert/<pk>/edit/', advert_edit, name='advert_edit'),

    path('company/', company_view, name='company_view'),
    path('company/add/', company_edit, name='company_add'),
    path('company/<pk>/edit/', company_edit, name='company_edit'),

    path('user/', user_view, name='user_view'),
    path('user_password/<pk>/', user_password, name='user_password'),
    path('user/add/', user_edit, name='user_add'),
    path('user/<pk>/edit/', user_edit, name='user_edit'),

    path('go/', news_short_list, name='news_short_list'),
    path('go/<short>/', news_short, name='news_short'),
    path('detail/<pk>/', news_detail, name='news_detail'),

    # Проверка на плагиат
    path('content/<pk>/', post_content, name='post_content'),

    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    path('feed/', PostFeed()),
    path('rss/yandex/', YandexRss.as_view(), name='rss'),
    path('rss/zen/', YandexDzenRss.as_view(), name='zen'),
    path('rss/turbo/', YandexTurboRss.as_view(), name='turbo'),
    path('robots.txt', robots),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
