from django.contrib import admin
from people.models import Company, User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'email', 'gender', )
    list_filter = ('changed', )
    fieldsets = (
        (None, {'fields': ('company', 'username', 'email', 'role', 'gender', )}),
    )


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ('title', 'label', )
    list_filter = ('changed', )
    fieldsets = (
        (None, {'fields': ('title', 'site', 'domain', 'folder', 'site_name', 'description', 'keywords', 'label')}),
    )
