from django.db import models
from django.contrib.auth.models import UserManager as AbstractUserManager


class CompanyManager(models.Manager):
    """
    Менеджер компаний.
    """
    def for_user(self, user, company=None):
        from people.models import User
        if isinstance(user, User) and user.is_superuser:
            return self.filter(id=user.company_id)

        if isinstance(user, User) and user.is_staff:
            return self.filter(id=user.company_id)

        if company:
            return self.filter(id=company)
        return self.none()


class UserManager(AbstractUserManager):
    """
    Менеджер людей.
    """
    def get_by_natural_key(self, username):
        return self.get(username=username)

    def get_queryset(self):
        return super().get_queryset()

    def for_user(self, user, company=None):
        if user.is_superuser:
            return self.filter(company=user.company)

        if user.is_staff:
            return self.filter(company=user.company)

        if company:
            return self.filter(company=user.company)

        return self.none()
