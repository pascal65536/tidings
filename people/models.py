import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import ForeignKey, UUIDField, CharField, DateTimeField

from people.managers import CompanyManager, UserManager


class Company(models.Model):

    title = CharField('Title', max_length=512)
    slug = UUIDField(verbose_name='Публичный код', default=uuid.uuid4, db_index=True, unique=True, editable=False)
    site = CharField('Site', max_length=512)
    site_name = CharField('Site name', max_length=512)
    folder = CharField('Folder', max_length=512)
    domain = CharField('Domain', max_length=512)
    description = CharField('Description', blank=True, null=True, max_length=512)
    keywords = CharField('Keywords', blank=True, null=True, max_length=512)
    label = CharField('Label', blank=True, null=True, max_length=512)
    created = DateTimeField('Created', auto_now_add=True)
    changed = DateTimeField('Changed', auto_now=True)
    deleted = DateTimeField('Deleted', blank=True, null=True, help_text='Set datetime, when it was deleted')

    objects = CompanyManager()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Компания'
        verbose_name_plural = 'Компании'
        ordering = ['title']


class User(AbstractUser):

    GENDER_CHOICE = [
        ('male', 'Male'),
        ('female', 'Female'),
    ]
    
    ROLE_CHOICE = [
        ('reader', 'Reader'),
        ('editor', 'Editor'),
        ('advertiser', 'Advertiser'),
        ('photographer', 'Photographer'),
    ]

    REQUIRED_FIELDS = []
    USERNAME_FIELD = 'username'

    company = ForeignKey(Company, related_name='user_company',
                         blank=True, null=True, verbose_name='Компания', on_delete=models.SET_NULL)
    phone = CharField(max_length=32, blank=True, null=True)
    role = CharField(max_length=30, null=True, blank=True, choices=ROLE_CHOICE)
    gender = CharField(max_length=100, null=True, blank=True, choices=GENDER_CHOICE)
    created = DateTimeField('Created', auto_now_add=True)
    changed = DateTimeField('Changed', auto_now=True)
    deleted = DateTimeField('Deleted', blank=True, null=True, help_text='Set datetime, when it was deleted')

    objects = UserManager()

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = 'Человек'
        verbose_name_plural = 'Люди'
        ordering = ['-password']
