from ckeditor.widgets import CKEditorWidget
from django import forms

from people.models import Company, User
from postapp.models import Post, Charter, Tag


class SearchForm(forms.Form):
    """
    Поисковый запрос
    """
    search = forms.CharField(label=u'Search', required=False)

    def clean(self):
        cd = super(SearchForm, self).clean()
        return cd


class PostForm(forms.ModelForm):
    """
    Форма редактирования поста
    """
    text = forms.CharField(widget=CKEditorWidget())

    def __init__(self, *args, **kwargs):
        init_dct = kwargs.pop('init_dct', None)
        photo_list = init_dct['photo_list']
        charter_list = init_dct['charter_list']
        company_list = init_dct['company_list']
        super().__init__(*args, **kwargs)

        self.fields['company'].widget.attrs['class'] = 'select2'
        self.fields['company'].widget.choices = company_list

        self.fields['photo'].widget.attrs['class'] = 'select2'
        self.fields['photo'].widget.choices = photo_list

        self.fields['charter'].widget.attrs['class'] = 'select2'
        self.fields['charter'].widget.choices = charter_list

        self.fields['date_post'].widget.attrs.update({
            'class': 'form_datetime',
            'required': 'required',
            'data-provide': 'datetimepicker',
            'data-date-format': 'dd.mm.yyyy hh:ii:ss',
        })

        self.fields['deleted'].widget.attrs.update({
            'class': 'form_datetime',
            'data-provide': 'datetimepicker',
            'data-date-format': 'dd.mm.yyyy hh:ii:ss',
        })

    class Meta:
        model = Post
        exclude = ['meta_description', 'meta_keywords', 'meta_title', 'changed',
                   'created', 'og_picture', 'picture']


class CharterForm(forms.ModelForm):
    """
    Форма редактирования рубрики
    """
    def __init__(self, *args, **kwargs):
        init_dct = kwargs.pop('init_dct', None)
        company_list = init_dct['company_list']
        super().__init__(*args, **kwargs)

        self.fields['company'].widget.attrs['class'] = 'select2'
        self.fields['company'].widget.choices = company_list

    class Meta:
        model = Charter
        exclude = ['lead', 'meta_description', 'meta_keywords', 'meta_title', 'og_picture', 'slug']


class UserForm(forms.ModelForm):
    """
    Форма для редактирования людей
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        model = User
        exclude = ['password', 'last_login', 'date_joined', 'groups', 'user_permissions', 'deleted']


class CompanyForm(forms.ModelForm):
    """
    Форма для редактирования компаний
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        model = Company
        exclude = []
