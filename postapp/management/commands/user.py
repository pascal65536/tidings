import string
import random

from django.core.management import BaseCommand

from people.models import User


def random_digits(range_start, range_end):
    return random.randint(range_start, range_end)


def random_with_N_digits(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return random.randint(range_start, range_end)


def create_random_string(length):
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str.capitalize()


def create_random_email():
    email = '{}@{}.{}'.format(create_random_string(random.randint(5, 10)),
                              create_random_string(random.randint(3, 8)),
                              create_random_string(random.randint(2, 3)))
    return email.lower()


def out_red(text):
    print(f"\033[31m {text} \033[37m")


def out_green(text):
    print(f"\033[32m {text} \033[37m")


def out_yellow(text):
    print(f"\033[33m {text} \033[37m")


def out_blue(text):
    print(f"\033[34m {text} \033[37m")


def out_magenta(text):
    print(f"\033[35m {text} \033[37m")


def out_cian(text):
    print(f"\033[36m {text} \033[37m")


# python manage.py user
class Command(BaseCommand):

    def handle(self, *args, **options):
        i = 100
        while i > 0:
            i -= 1
            out_green('=' * 80)
            first_name = create_random_string(10)
            last_name = create_random_string(10)
            user = User()
            user.email = create_random_email()
            user.username = first_name.lower()
            user.first_name = first_name
            user.last_name = last_name
            user.company_id = random.randint(1, 2)
            user.save()
