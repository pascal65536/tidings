from django.conf import settings
from django.core.management import BaseCommand
from people.models import Company


# python manage.py setup
class Command(BaseCommand):

    def handle(self, *args, **options):
        p = Company()
        p.title = input("Введите title: ")
        # p.slug = charter
        p.site = input("Введите site: ")
        p.site_name = input("Введите site_name: ")
        p.folder = input("Введите folder: ")
        p.domain = input("Введите domain: ")
        p.description = input("Введите description: ")
        p.keywords = input("Введите keywords: ")
        p.label = input("Введите label: ")
        p.save()
