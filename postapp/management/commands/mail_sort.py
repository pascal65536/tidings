import json
import os
import random
import uuid
import email
import imaplib
from datetime import datetime
from time import sleep

from django.utils import timezone
from django.core.management import BaseCommand
from django.conf import settings

from email.utils import parsedate_tz, parsedate_to_datetime


EXT_DCT = {
    'text/plain': 'txt',
    'image/heic': 'heif',
    'image/webp': 'webp',
    'image/gif': 'gif',
    'text/html': 'html',
    'image/jpeg': 'jpg',
    'image/tiff': 'tiff',
    'image/png': 'png',
    'text/csv': 'csv',
    'application/ics': 'ics',
    'application/octet-stream': 'exe',
    'application/msword': 'docx',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document': 'doc',
    'application/vnd.ms-word.document.12': 'doc',
    'application/vnd.oasis.opendocument.text': 'odt',
    'video/x-matroska': 'mkv',
    'application/pdf': 'pdf',
    'application/vnd.ms-excel': 'xls',
    'application/emz': 'emz',
    'application/jpeg': 'jpeg',
    'application/zip': 'zip',
    'video/mp4': 'mp4',
    'text/rtf': 'rtf',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation': 'pptx',
}

EXT_LST = ['json', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'jpg', 'jpeg', 'png', 'bmp', 'tiff', 'zip', 'rar', 'xml']

# case "doc": return "application/msword";
# case "dot": return "application/msword";
# case "docx": return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
# case "dotx": return "application/vnd.openxmlformats-officedocument.wordprocessingml.template";
# case "docm": return "application/vnd.ms-word.document.macroEnabled.12";
# case "dotm": return "application/vnd.ms-word.template.macroEnabled.12";
# case "xls": return "application/vnd.ms-excel";
# case "xlt": return "application/vnd.ms-excel";
# case "xla": return "application/vnd.ms-excel";
# case "xlsx": return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
# case "xltx": return "application/vnd.openxmlformats-officedocument.spreadsheetml.template";
# case "xlsm": return "application/vnd.ms-excel.sheet.macroEnabled.12";
# case "xltm": return "application/vnd.ms-excel.template.macroEnabled.12";
# case "xlam": return "application/vnd.ms-excel.addin.macroEnabled.12";
# case "xlsb": return "application/vnd.ms-excel.sheet.binary.macroEnabled.12";
# case "ppt": return "application/vnd.ms-powerpoint";
# case "pot": return "application/vnd.ms-powerpoint";
# case "pps": return "application/vnd.ms-powerpoint";
# case "ppa": return "application/vnd.ms-powerpoint";
# case "pptx": return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
# case "potx": return "application/vnd.openxmlformats-officedocument.presentationml.template";
# case "ppsx": return "application/vnd.openxmlformats-officedocument.presentationml.slideshow";
# case "ppam": return "application/vnd.ms-powerpoint.addin.macroEnabled.12";
# case "pptm": return "application/vnd.ms-powerpoint.presentation.macroEnabled.12";
# case "potm": return "application/vnd.ms-powerpoint.template.macroEnabled.12";
# case "ppsm": return "application/vnd.ms-powerpoint.slideshow.macroEnabled.12";
# case "mdb": return "application/vnd.ms-access";


def create_json(root_folder, days=30):

    find_file_lst = list()
    for root, dirs, files in os.walk(root_folder):
        find_file_lst += [os.path.join(root, name) for name in files]

    ret_dct = dict()
    for find_file in find_file_lst:
        find_lst = find_file.split('/')
        if len(find_lst) == 9:
            email_txt, data_txt, file_txt = find_file.split('/')[6:]
            data_str, _ = data_txt.split('_')
            # date_datetime = data_str.strftime('%Y-%m-%d-%H-%M-%S')
            date_datetime = datetime.strptime(data_str, '%Y-%m-%d-%H-%M-%S')
            if (timezone.now().date() - date_datetime.date()) > timezone.timedelta(days=days):
                continue
            ret_dct.setdefault(email_txt, {}).setdefault(data_txt, {}).setdefault('files', [])
            ret_dct[email_txt][data_txt]['files'].append(file_txt)

    return ret_dct


def save_payload(content, full_folder):
    # Проверим аттачи
    # attachment_lst = content.get_payload()
    # if isinstance(attachment_lst, str):
    #     return None

    if content.get_content_type() in ['multipart/alternative', 'multipart/mixed', 'multipart/related']:
        for cc in content._payload:
            save_payload(cc, full_folder)
        return None

    filename = f'{str(uuid.uuid4())}.{EXT_DCT[content.get_content_type()]}'
    data = content.get_payload(decode=True)
    if data:
        full_filename = os.path.join(full_folder, filename)
        with open(full_filename, 'wb') as f:
            f.write(data)
    return None


def download_files(root_folder):
    print('download_files')
    mail = imaplib.IMAP4_SSL(settings.EMAIL_FETCH['email_host'], 993)
    mail.login(settings.EMAIL_FETCH['email_user'], settings.EMAIL_FETCH['email_pass'])
    mail.list()
    # Выводит список папок в почтовом ящике.
    mail.select('inbox')
    # Возьмём непрочитанные сообщения
    result, data_b = mail.uid('search', None, 'ALL')  # ALL / SEEN / UNSEEN
    if not result == 'OK':
        print('No Unread Emails')
        return None
    print(data_b)
    i = 1
    for latest_email_uid in reversed(data_b[0].split()):
        i += 1
        # Помечаем прочитанным
        mail.uid('STORE', latest_email_uid, '+FLAGS', '\SEEN')
        result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
        email_message = email.message_from_bytes(data[0][1])
        date_mail = parsedate_to_datetime(email_message['Date'])
        subject_mail, from_mail = email.utils.parseaddr(email_message['From'])
        year_str = date_mail.strftime('%Y')
        month_str = date_mail.strftime('%m')
        day_str = date_mail.strftime('%d')
        date_str = f"{date_mail.strftime('%Y-%m-%d-%H-%M-%S')}_{latest_email_uid.decode('utf-8')}"
        full_folder = os.path.join(root_folder, year_str, month_str, day_str, from_mail.lower(), date_str)
        if not os.path.exists(full_folder):
            os.makedirs(full_folder)
            save_payload(email_message, full_folder)
            print('>' * 80)
            # mail.uid('STORE', latest_email_uid, '+FLAGS', '(\\Deleted)')
        print(date_mail, result, from_mail)
        if i > 50:
            import ipdb; ipdb.sset_trace()
    mail.expunge()
    mail.close()
    mail.logout()


def update_email(fail_set, folder_name='0_fail_folder', is_read=True):
    mail = imaplib.IMAP4_SSL(settings.EMAIL_FETCH['email_host'], 993)
    mail.login(settings.EMAIL_FETCH['email_user'], settings.EMAIL_FETCH['email_pass'])
    mail.list()
    mail.select('inbox')
    for uid in fail_set:
        mail.create(folder_name)
        print(uid)
        if is_read:
            mail.uid('STORE', uid, '-FLAGS', '(\SEEN)')
        else:
            mail.uid('STORE', uid, '+FLAGS', '(\SEEN)')
        mail.uid('COPY', uid, folder_name)
        mail.uid('STORE', uid, '+FLAGS', '(\Deleted)')

    mail.expunge()
    mail.close()
    mail.logout()
    return None


def update_app(file_json, email_lst, days=30):
    ret = {'success': set(), 'fail': set(), 'unknown': set()}
    for email_txt, date_uid_dct in file_json.items():
        for date_uid in date_uid_dct.keys():
            data_str, uid = date_uid.split('_')
            if email_txt not in email_lst:  # Почты нет в списке, сохраняем номера писем для дальнейшего удаления
                ret['fail'].add(uid)
                continue

            date_datetime = datetime.strptime(data_str, '%Y-%m-%d-%H-%M-%S')
            if (timezone.now().date() - date_datetime.date()) > timezone.timedelta(days=days):
                ret['fail'].add(uid)
                continue

            for file_path in date_uid_dct[date_uid]['files']:
                _, ext = file_path.split('.')
                if ext in EXT_LST:
                    print(file_path)
                    ret['success'].add(uid)

            if uid not in ret['success']:
                ret['unknown'].add(uid)

    return ret


def check_app(days=30):
    status_max = 5000  # Будем брать Request в статусах, когда можно добавить документы
    status_min = 10  # Будем брать Request в статусах, когда можно добавить документы

    # request_qs = Request.objects.in_work(days=days).filter(status_flow__lte=status_max, status_flow__gte=status_min)
    request_qs = Request.objects.all()
    email_dct = dict()
    for request_email, request_id in request_qs.order_by('email', '-created').values_list('email', 'id'):
        email_lst = email_dct.setdefault(request_email.lower(), [])
        email_lst.append(request_id)
    return email_dct


class Command(BaseCommand):
    """
    Документы будут добавлены к Request созданным за последние 7 дней в статусах от 10 до 5000.
    Настройки для почтового ящика, на который надо слать документы хранятся в settings_local
    EMAIL_FETCH = {
        'email_host': 'imap.yandex.ru',
        'email_user': 'devnull@mail.ua',
        'email_pass': 'password',
        'export_path': 'media/export_path/',
        'success_folder': 'success_folder',
        'fail_folder': 'fail_folder',
    }
    В почтовом ящике для документов будут обработаны все письма за 7 дней. Если для адреса отправителя найдётся Request,
    и в письме есть вложения и они json или pdf, то найдём последний Request с такой почтой, создадим для него Документ
    и сохраним его.

    Проверять почту, принимать документы от пользователей, чьи ящики есть в БО.
    Обработанные письма складывать в одну папку, необработанные в другую.

    Необходимо включить доступ небезопасным приложениям по ссылке
    https://myaccount.google.com/lesssecureapps

    Включить IMA
    https://mail.google.com/mail/u/3/#settings/fwdandpop

    Добавить пароль для приложения
    https://passport.yandex.com/profile/

    >> python manage.py mail_fetcher

    """
    help = 'Mail Fetcher'

    def handle(self, *args, **options):
        root_folder = '/tmp/email'
        email_lst = ['zheleznitskayaee@dialog-regions.ru', 'kev@gornovosti.ru',
                     'ab@simple-sport.ru', 'ak@gornovosti.ru']
        while True:
            print('Check email')
            # email_dct = check_app()
            # print(email_dct)
            # download_files(root_folder)
            ret_json = create_json(root_folder)
            with open(os.path.join(root_folder, 'email.json'), 'w') as f:
                json.dump(ret_json, f, ensure_ascii=False, sort_keys=True)
            ret = update_app(ret_json, email_lst)
            update_email(ret['unknown'], settings.EMAIL_FETCH['fail_folder'], is_read=False)
            update_email(ret['fail'], settings.EMAIL_FETCH['fail_folder'], is_read=True)
            update_email(ret['success'], settings.EMAIL_FETCH['success_folder'], is_read=False)
            import ipdb; ipdb.sset_trace()
            sleep(random.randint(30, 90))

            # # Проверим есть ли такая почта среди оставивших заявку
            # if from_mail not in email_dct.keys():
            #     continue

            # # Сколько секунд назад пришло письмо
            # total_seconds = (localtime(now()).replace(tzinfo=None) - date_mail.replace(tzinfo=None)).total_seconds()

            # # Если очень давно, то прекращаем спрашивать
            # if total_seconds > 24 * 60 * 60 * days:
            #     continue

            # # Проверим аттачи
            # attachment_lst = email_message.get_payload()
            # if isinstance(attachment_lst, str):
            #     continue

        #     dst = settings.EMAIL_FETCH['fail_folder']
        #     try:
        #         dst = settings.EMAIL_FETCH['success_folder']
        #         for msg in attachment_lst:
        #             if msg is None:
        #                 continue
        #             for part in msg.walk():
        #                 if part.get_content_maintype() == 'multipart':
        #                     continue
        #                 if part.get('Content-Disposition') is None:
        #                     continue
        #
        #                 ext = EXT_DCT.get(part.get_content_type(), 000)
        #                 if ext not in ext_lst:
        #                     dst = settings.EMAIL_FETCH['fail_folder']
        #                     continue
        #
        #                 file_name = f'{date_mail.strftime("%Y-%m-%d-%H-%M-%S")}.{ext}'
        #                 request_obj = Request.objects.get(id=email_dct[from_mail][0])  # Кандидат на удаление
        #                 document_dct = {
        #                     'user_id': request_obj.user.id,
        #                     'request': request_obj,
        #                     'title': file_name,
        #                     'category': 42,
        #                     'uid': uuid.uuid4(),
        #                     'original': None,
        #                     'description': 'Received from the Client by email',
        #                     'valid': 1,
        #                 }
        #
        #                 doc = None
        #                 ret = Document.creator(data=[document_dct])
        #                 if len(ret['results']) > 0:
        #                     doc = ret['results'][0]
        #                     doc.original.save(file_name, ContentFile(part.get_payload(decode=True)), save=True)
        #                     doc.save()
        #
        #                 if doc:
        #                     for request_id in email_dct[from_mail]:
        #                         if RequestDocument.objects.filter(request_id=request_id, document=doc).count():
        #                             continue
        #                         rd_data = {
        #                             'request': Request.objects.get(id=request_id),
        #                             'document': doc,
        #                             'valid': doc.valid,
        #                         }
        #                         RequestDocument.creator(data=[rd_data], smart=True, auto_version=True)
        #     finally:
        #         mail.create(dst)
        #         mail.uid('STORE', latest_email_uid, '-FLAGS', '\SEEN')
        #         mail.uid('COPY', latest_email_uid, dst)
        #         mail.uid('STORE', latest_email_uid, '+FLAGS', '(\Deleted)')
        #         mail.expunge()
        #
        #     print(latest_email_uid, dst)
        #
        # if hasattr(settings, 'FILE_STOP_TASKS') and Path(settings.FILE_STOP_TASKS).exists():
        #     print(f'Exiting... because, file {settings.FILE_STOP_TASKS} exists!')
        #     break
