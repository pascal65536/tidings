import random
from django.conf import settings
from django.core.management import BaseCommand
from django.utils import timezone

from people.models import Company
from photoapp.models import Photo
from postapp.models import Charter, Post


def generate_password(n):
    password_str = ''
    for i in range(n):
        password_str = password_str + random.choice(settings.CHARSET_LST)
    return password_str


# python manage.py new_tags
class Command(BaseCommand):

    def handle(self, *args, **options):
        photo = Photo.objects.last()
        company = Company.objects.last()
        charter = Charter.objects.last()

        post = Post.objects.all()
        print(post[0].get_short())

        for i in range(1, 100):
            p = Post()
            p.company = company
            p.charter = charter
            p.photo = photo
            p.date_post = timezone.now()
            p.title = generate_password(10)
            p.lead = f'{generate_password(14)} {generate_password(2)} {generate_password(10)}'
            p.text = f'{generate_password(15)} {generate_password(4)} ' \
                     f'{generate_password(12)} {generate_password(5)} ' \
                     f'{generate_password(13)} {generate_password(6)} ' \
                     f'{generate_password(11)}'
            p.save()

