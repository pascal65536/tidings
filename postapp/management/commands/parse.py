import random
from django.core.files import File
from django.core.management.base import BaseCommand
from lxml.html import fromstring
from lxml import etree
from urllib.request import urlopen, urlretrieve
from people.models import Company
from photoapp.models import Photo
from postapp.models import Post, Tag, Charter


def parse_post(list_doc):
    ret_dct = {
        'params_json': {
            'images': [],
            'author': None,
            'url': None
        }
    }

    for ll in list_doc.cssselect('div.tgme_widget_message.err_message.js-widget_message div.tgme_widget_message_bubble div.tgme_widget_message_error'):
        if ll.text_content() == 'Post not found':
            return None

    # Автор
    for ll in list_doc.cssselect('div.tgme_widget_message_author.accent_color'):
        ret_dct['params_json']['author'] = ll.cssselect('span')[0].text_content()

    # Источник
    for ll in list_doc.cssselect('div.tgme_widget_message.js-widget_message div.tgme_widget_message_bubble div.tgme_widget_message_forwarded_from.accent_color'):
        ret_dct['params_json']['from_text'] = ll.text_content()
        ret_dct['params_json']['from_url'] = ll.cssselect('a')[0].get('href') if ll.cssselect('a') else None
        ret_dct['params_json']['from_name'] = ll.cssselect('span')[0].text_content()

    # Фото
    for ll in list_doc.cssselect('div.tgme_widget_message_grouped.js-message_grouped div.tgme_widget_message_grouped_layer.js-message_grouped_layer'):
        for a in ll.cssselect('a'):
            if 'https' in a.get('style'):
                ret_dct['params_json']['images'].append(f"https{a.get('style').split('https')[1]}"[:-2])
            else:
                ret_dct['params_json']['images'].append(f"https{a.cssselect('i')[0].get('style').split('https')[1]}"[:-2])

    # Content
    for ll in list_doc.cssselect('div.tgme_widget_message.js-widget_message div.tgme_widget_message_bubble'):
        for a in ll.cssselect('a'):
            if a.get('style') and 'https' in a.get('style'):
                ret_dct['params_json']['images'].append(f"https{a.get('style').split('https')[1]}"[:-2])

    # Тело поста
    for ll in list_doc.cssselect('div.tgme_widget_message.js-widget_message div.tgme_widget_message_bubble div.tgme_widget_message_text.js-message_text'):
        ret_dct['text'] = etree.tounicode(ll, method='html')
        if ll.text:
            ret_dct['title'] = ' '.join(ll.text.split(' ')[:7])
            ret_dct['lead'] = ' '.join(ll.text.split('/n')[:1])

    # Ссылка
    for ll in list_doc.cssselect('div.tgme_widget_message_link.accent_color a.link_anchor.flex_ellipsis'):
        ret_dct['params_json']['url'] = ll.cssselect('a')[0].get('href')

    # Число просмотров
    for ll in list_doc.cssselect('div.tgme_widget_message.js-widget_message div.tgme_widget_message_bubble div.tgme_widget_message_footer.js-message_footer div.tgme_widget_message_info.js-message_info span.tgme_widget_message_views'):
        ret_dct['params_json']['view'] = ll.cssselect('span')[0].text_content()

    # Дата публикации
    for ll in list_doc.cssselect('span.tgme_widget_message_meta a.tgme_widget_message_date time.datetime'):
        ret_dct['date_post'] = ll.get('datetime')

    return ret_dct


# python manage.py parse
class Command(BaseCommand):
    def handle(self, *args, **options):
        charter = Charter.objects.get(title='Телеграм')
        company = Company.objects.get(id=1)

        # for tg in Post.objects.filter(photo__isnull=True):
        #     print(tg)
        #     photo_idx = set(Post.objects.filter(photo__isnull=True).values_list('photo_id', flat=True))
        #     img = Photo.objects.exclude(id__in=photo_idx).first()
        #     Post.objects.filter(id=tg.pk).update(photo=img)

        while True:
            sleep = random.randint(1, 60)
            # print(f'Sleep {sleep} sec.')
            # time.sleep(sleep)
            # with open('channel_lst.json', encoding='utf-8') as fh:
            #     channel_lst = json.load(fh)
            for channel in ["gornovosti", "borusio"]:
                # tg = Post.objects.filter(url__icontains=channel).order_by('-date_post').first()
                # post = tg.post + random.randint(-1 * tg.post, 1000) if tg else random.randint(1, 1000)
                post = random.randint(1, 1000)
                parse_url = f'https://t.me/{channel}/{post}?embed=2'
                f = urlopen(parse_url)
                list_html = f.read().decode('utf-8')
                list_doc = fromstring(list_html)
                rez = parse_post(list_doc)

                if not rez:
                    continue
                if not rez['params_json']['author']:
                    continue

                rez['params_json']['post'] = post
                rez['params_json']['channel'] = channel

                tg, is_new = Post.objects.get_or_create(title=rez['params_json']['url'],
                                                        charter=charter,
                                                        company=company,
                                                        defaults=rez)
                print(tg)
                img = None
                for url in set(rez['params_json']['images']):
                    result = urlretrieve(url)
                    img = Photo()
                    img.image_url = url
                    img.company = company
                    img.title = rez.get('title') or f'{channel} #{post}'
                    img.picture.save('tmp2.jpg', File(open(result[0], 'rb')), save=True)
                    img.save()
                if not img:
                    photo_idx = set(Post.objects.filter(photo__isnull=True).values_list('photo_id', flat=True))
                    img = Photo.objects.exclude(id__in=photo_idx).last()
                if not tg.photo:
                    tg.photo = img
                    tg.save()
