from django.contrib import admin
from postapp.models import Post, Charter, Tag, TagPost


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'image_img', 'charter', 'date_post', )
    list_filter = ('charter', )
    fieldsets = (
        (None, {
            'fields': ('title', 'lead', 'text', 'company', )
        }),
        ('Фильтрация', {
            'fields': ('charter', )
        }),
        ('Изображения', {
            'fields': ('photo', )
        }),
        ('Даты', {
            'fields': ('date_post', 'deleted', )
        }),
        ('SEO', {
            'fields': ('og_picture', 'meta_title', 'meta_keywords', 'meta_description', )
        }),
    )


@admin.register(Charter)
class CharterAdmin(admin.ModelAdmin):
    list_display = ('title', 'lead', 'order')
    fieldsets = (
        (None, {
            'fields': ('title', 'lead', 'company', )
        }),
        ('Фильтрация', {
            'fields': ('order',)
        }),
        ('Изображения', {
            'fields': ('picture', )
        }),
        ('SEO', {
            'fields': ('og_picture', 'meta_title', 'meta_keywords', 'meta_description')
        }),
    )


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', )


@admin.register(TagPost)
class TagPostAdmin(admin.ModelAdmin):
    list_display = ('tag', 'post', 'num', )
