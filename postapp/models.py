from autoslug import AutoSlugField
from ckeditor.fields import RichTextField
from django.contrib.syndication.views import Feed
from django.db import models
from django.conf import settings
from django.db.models import CharField, IntegerField, TextField, ForeignKey, ImageField, DateTimeField, BooleanField, \
    JSONField
from django.utils import timezone
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.views.generic import TemplateView
from django.shortcuts import get_object_or_404
from autoslug.settings import slugify

from newsapp.utils import latin_filename, opengraph, get_settings, delete_tags, process_text
from people.models import Company
from photoapp.models import Photo
from postapp.management.commands.feed import get_clean_text
from postapp.managers import PostManager, CharterManager


class Charter(models.Model):
    title = CharField(verbose_name='Название', max_length=20)
    company = ForeignKey(Company, related_name='charter_company',
                         blank=True, null=True, verbose_name='Компания', on_delete=models.SET_NULL)
    lead = TextField(verbose_name='Лидер-абзац')
    order = IntegerField(verbose_name='Сортировка', default=1)
    slug = AutoSlugField(populate_from='title')
    picture = ImageField(verbose_name='Картинка раздела', upload_to=latin_filename, blank=True, null=True)
    og_picture = CharField(verbose_name='Картинка для соцсетей', max_length=255, blank=True)
    meta_title = CharField(max_length=255, verbose_name=u'Title', null=True, blank=True)
    meta_keywords = CharField(max_length=255, verbose_name=u'Keywords', null=True, blank=True)
    meta_description = TextField(max_length=255, verbose_name=u'Description', null=True, blank=True)

    objects = CharterManager()

    def get_short(self):
        res_lst = list()
        n = self.pk * settings.SALT
        m = len(settings.CHARSET_LST)
        while n > 0:
            res_lst.append(settings.CHARSET_LST[n % m])
            n = n // m
        return "".join(res_lst)

    def save(self, *args, **kwargs):
        if self.pk:
            self.og_picture = opengraph(self)
        super(Charter, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.title} | {self.company}'

    class Meta:
        verbose_name = 'Название раздела'
        verbose_name_plural = 'Названия разделов'
        ordering = ['-order']


class Post(models.Model):
    params_json = JSONField(verbose_name='params_json')
    title = CharField(verbose_name='Заголовок поста', max_length=255)
    lead = TextField(verbose_name='Лидер-абзац', blank=True, null=True)
    text = RichTextField(verbose_name='Текст поста', blank=True, null=True)
    company = ForeignKey(Company, related_name='post_company',
                         blank=True, null=True, verbose_name='Компания', on_delete=models.SET_NULL)
    charter = ForeignKey(Charter, related_name='post_charter',
                         blank=True, null=True, verbose_name='Раздел', on_delete=models.SET_NULL)
    photo = ForeignKey(Photo, blank=True, null=True, verbose_name='Фото', on_delete=models.SET_NULL)
    date_post = DateTimeField(verbose_name='Дата публикации')
    og_picture = CharField(verbose_name='Картинка для соцсетей', max_length=255, blank=True)

    created = DateTimeField(verbose_name='Создано', auto_now_add=True)
    changed = DateTimeField(verbose_name='Изменено', auto_now=True)
    deleted = DateTimeField(verbose_name='Удалено', blank=True, null=True)
    meta_title = CharField(max_length=255, verbose_name='Title', null=True, blank=True)
    meta_keywords = CharField(max_length=255, verbose_name='Keywords', null=True, blank=True)
    meta_description = TextField(max_length=255, verbose_name='Description', null=True, blank=True)
    turbo = BooleanField(verbose_name=u'Яндекс Турбо', default=True)

    objects = PostManager()

    def image_img(self):
        """
        Вывод картинок в админке
        """
        if self.photo:
            return mark_safe(u'<a href="{0}" target="_blank"><img src="{0}" width="100"/></a>'.format(self.photo.picture.url))
        else:
            return '(Нет изображения)'

    def save_tags(self):
        plain_list = set()
        with open('dictionary/word_rus.txt', 'r') as fl:
            for line in fl:
                plain_list.add(line.strip().upper())
        alphabet = 'йцукенгшщзхъёфывапролджэячсмитьбю'
        backspase = ['    ', '   ', '  ']

        text = get_clean_text(self.text)
        new_text = ''
        for t in text:
            new_text += t if t in alphabet or t in alphabet.upper() else ' '

        for bs in backspase:
            new_text = new_text.replace(bs, ' ')

        tags_lst = new_text.upper().split(' ')
        tags = plain_list & set(tags_lst)
        tagpost_lst = list(set(TagPost.objects.filter(post=self).values_list('tag__title', flat=True)))

        for tag in tags:
            tag_obj, _ = Tag.objects.get_or_create(title=tag)
            num = tags_lst.count(tag)
            if tag in tagpost_lst:
                tagpost_lst.remove(tag)
            tagpost_obj, is_new = TagPost.objects.get_or_create(tag=tag_obj, post=self, defaults={'num': num})
            if not is_new:
                TagPost.objects.filter(id=tagpost_obj.id).update(num=num)

        for delete_tag in tagpost_lst:
            TagPost.objects.filter(tag__title=delete_tag, post=self).delete()
        return tags

    @classmethod
    def get_tags(cls, post_qs, keyword_str):
        post_ids = set(post_qs.values_list('id', flat=True))
        ret = set(TagPost.objects.filter(id__in=post_ids).values_list('tag__title', flat=True))
        ret.update(set([keyword.strip().capitalize() for keyword in keyword_str.split(',')]))
        return list(ret)

    def get_absolute_url(self):
        return reverse('news_detail', kwargs={'pk': self.pk})

    def get_short(self):
        res_lst = list()
        n = self.pk * settings.SALT
        m = len(settings.CHARSET_LST)
        while n > 0:
            res_lst.append(settings.CHARSET_LST[n % m])
            n = n // m
        return ''.join(res_lst)

    @classmethod
    def update_qs(cls, news_qs):
        photo_idx = set()
        charter_idx = set()

        for news in news_qs:
            photo_idx.add(news.photo_id)
            charter_idx.add(news.charter_id)

        photos = Photo.objects.in_bulk(photo_idx)
        charters = Charter.objects.in_bulk(charter_idx)

        for news in news_qs:
            news.delayed = False
            if news.date_post > timezone.now():
                news.delayed = True

            news.photo_picture = None
            if news.photo:
                news.photo_picture = photos[news.photo_id].picture

            news.charter_title = None
            news.charter_slug = None
            if news.charter:
                news.charter_title = charters[news.charter_id].title
                news.charter_slug = charters[news.charter_id].slug
        return news_qs

    def save(self, *args, **kwargs):
        if self.pk:
            self.og_picture = opengraph(self)
        super(Post, self).save(*args, **kwargs)

    @property
    def meta(self):
        tags_set = set(TagPost.objects.filter(post=self).values_list('tag__title', flat=True))
        meta_keywords_lst = [tag for tag in tags_set]
        meta_keywords = ', '.join(meta_keywords_lst)

        return {
            'title': f'{self.meta_title or self.title} » {self.charter.title} « {get_settings().get("title")}',
            'keywords': self.meta_keywords or meta_keywords,
            'description': self.meta_description or self.lead,
        }

    def __str__(self):
        return f'{self.title} | {self.charter}'

    class Meta:
        verbose_name = 'Запись в блог'
        verbose_name_plural = 'Записи в блог'
        ordering = ['-id']


class Tag(models.Model):
    title = CharField(verbose_name='Название', max_length=20)
    slug = models.CharField(verbose_name='Тег', max_length=255)
    menu = models.BooleanField(verbose_name='Назначить кнопкой меню', default=False)
    order = models.IntegerField(verbose_name='Сортировка', default=0)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        super(Tag, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Тег'
        verbose_name_plural = 'Теги'
        ordering = ['-title', '-slug']


class TagPost(models.Model):
    tag = ForeignKey(Tag, related_name='tagpost_tag',
                     blank=True, null=True, verbose_name='Тег', on_delete=models.SET_NULL)
    post = ForeignKey(Post, blank=True, null=True, verbose_name='Пост', on_delete=models.SET_NULL)
    num = IntegerField(verbose_name='Количество', default=0)

    def __str__(self):
        return str(self.pk)

    class Meta:
        verbose_name = 'Тег поста'
        verbose_name_plural = 'Теги постов'
        ordering = ['-tag', '-post']


class PostFeed(Feed):
    seo = get_settings()
    title = seo['title']
    description = seo['description']
    link = '/'

    def items(self):
        return Post.objects.filter(deleted__isnull=True, date_post__lte=timezone.now()).order_by('-date_post')[0:25]

    def item_title(self, obj):
        return obj.title

    def item_description(self, obj):
        return obj.lead

    def item_link(self, obj):
        return "/detail/%d" % obj.pk


class YandexRss(TemplateView):
    template_name = 'rss/yandex.xml'

    def get_context_data(self, **kwargs):
        company = Company.objects.filter(domain__contains=self.request.environ['HTTP_HOST'])[0]
        seo = get_settings(company.id)

        ctx = super(YandexRss, self).get_context_data(**kwargs)
        post_qs = Post.objects.for_user(self.request.user, company=company).filter(deleted__isnull=True, date_post__lte=timezone.now()).order_by('-date_post')[0:25]
        for post in post_qs:
            post.title = delete_tags(post.title)
            post.lead = '<![CDATA[{}]]>'.format(delete_tags(post.lead))
            post.text = '<![CDATA[{}]]>'.format(delete_tags(post.text))
        ctx['object_list'] = post_qs
        ctx['static'] = settings.STATIC_URL
        ctx['media'] = settings.MEDIA_URL
        ctx['host'] = seo['domain']
        ctx['sitename'] = seo['title']
        ctx['description'] = seo['description']
        return ctx

    def render_to_response(self, context, **response_kwargs):
        response_kwargs['content_type'] = 'text/xml; charset=UTF-8'
        return super(YandexRss, self).render_to_response(context, **response_kwargs)


class YandexDzenRss(TemplateView):
    template_name = 'rss/zen.xml'

    def get_context_data(self, **kwargs):
        company = Company.objects.filter(domain__contains=self.request.environ['HTTP_HOST'])[0]
        seo = get_settings(company.id)

        ctx = super(YandexDzenRss, self).get_context_data(**kwargs)
        post_qs = Post.objects.for_user(self.request.user, company=company).filter(deleted__isnull=True, date_post__lte=timezone.now()).order_by('-date_post')[0:25]
        for post in post_qs:
            post.title = delete_tags(post.title)
            post.lead = '<![CDATA[{}]]>'.format(delete_tags(post.lead))
            post.text = '<![CDATA[{}]]>'.format(delete_tags(post.text))
        ctx['object_list'] = post_qs
        ctx['static'] = settings.STATIC_URL
        ctx['media'] = settings.MEDIA_URL
        ctx['host'] = seo['domain']
        ctx['sitename'] = seo['title']
        ctx['description'] = seo['description']
        return ctx

    def render_to_response(self, context, **response_kwargs):
        response_kwargs['content_type'] = 'text/xml; charset=UTF-8'
        return super(YandexDzenRss, self).render_to_response(context, **response_kwargs)


class YandexTurboRss(TemplateView):
    template_name = 'rss/turbo.xml'

    def get_context_data(self, **kwargs):
        company = Company.objects.filter(domain__contains=self.request.environ['HTTP_HOST'])[0]
        seo = get_settings(company.id)

        ctx = super(YandexTurboRss, self).get_context_data(**kwargs)
        slug_charter = ctx['view'].request.GET.get('charter')
        slug_tag = ctx['view'].request.GET.get('tag')

        filter_dct = {
            # 'deleted__isnull': True,
            'date_post__lte': timezone.now(),
        }
        if slug_tag:
            filter_dct.update(
                {'tags__slug': slug_tag}
            )
            get_object_or_404(Tag, slug=slug_tag)

        if slug_charter:
            filter_dct.update(
                {'charter__slug': slug_charter}
            )
            get_object_or_404(Charter, slug=slug_charter)

        post_qs = Post.objects.for_user(self.request.user, company=company).filter(**filter_dct).order_by('-date_post')[0:50]
        for post in post_qs:
            post.turbo_new = post.turbo and not bool(post.deleted)
            post.title = process_text(post.title)
            post.lead = process_text(post.lead)
            post.text = process_text(post.text)
            post.url = post.get_absolute_url()
        ctx['object_list'] = post_qs
        ctx['static'] = settings.STATIC_URL
        ctx['media'] = settings.MEDIA_URL
        ctx['host'] = seo['domain']
        ctx['sitename'] = seo['title']
        ctx['description'] = seo['description']
        return ctx

    def render_to_response(self, context, **response_kwargs):
        response_kwargs['content_type'] = 'text/xml; charset=UTF-8'
        return super(YandexTurboRss, self).render_to_response(context, **response_kwargs)
