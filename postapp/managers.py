from django.db import models
from django.utils import timezone
from people.models import User


class PostManager(models.Manager):
    """
    Менеджер новостей.
    """
    def for_user(self, user, company=None):
        if isinstance(user, User) and user.is_superuser:
            return self.filter(company=user.company)
        if isinstance(user, User) and user.is_staff:
            return self.filter(company=user.company, text__isnull=False)
        if company:
            return self.filter(deleted__isnull=True, date_post__lte=timezone.now(), company=company, text__isnull=False)
        return self.filter(deleted__isnull=True, date_post__lte=timezone.now(), text__isnull=False, company=company)


class CharterManager(models.Manager):
    """
    Менеджер рубрик.
    """
    def for_user(self, user, company=None):
        if isinstance(user, User) and user.is_superuser:
            return self.filter(company=user.company)

        if isinstance(user, User) and user.is_staff:
            return self.filter(company=user.company)

        if company:
            return self.filter(company=company)
        return self.none()
