import random
import requests

from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import HttpResponse, Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.db.models import Q, Count
from django.template import loader, Context
from django.conf import settings

from newsapp.views import news_detail
from newsapp.utils import get_settings, cyr_lat, update_charter
from people.models import Company, User
from photoapp.models import Photo
from postapp.form import PostForm, CharterForm, CompanyForm, UserForm
from postapp.models import Post, Charter, Tag, TagPost


@login_required(login_url='/login/')
@staff_member_required
def post_edit(request, pk=None):
    """
    Редактирование поста
    """
    instance = None
    photo_list = [('', '---')]
    if pk:
        instance = get_object_or_404(Post, pk=pk)
        photo_list.append((instance.photo.id, instance.photo.title), )

    photo_qs = Photo.objects.for_user(request.user).order_by('-changed')[:10]
    for photo in photo_qs:
        photo_list.append((photo.id, photo.title), )

    charter_list = list()
    charter_qs = Charter.objects.for_user(request.user).order_by('-order')
    for charter in charter_qs:
        charter_list.append((charter.id, charter.title), )

    company = request.user.company
    init_dct = {
        'company_list': [(company.id, company.title)],
        'photo_list': photo_list,
        'charter_list': charter_list,
    }

    form = PostForm(data=request.POST or None, files=request.FILES or None, instance=instance, init_dct=init_dct)
    if request.method == 'POST' and form.is_valid():
        cd = form.cleaned_data
        post = form.save(commit=False)
        post.save()
        # Нельзя добавить теги к несуществующему объекту.
        post.save_tags()
        return redirect(post_edit, post.pk)

    tag_slugs = set(TagPost.objects.filter(post=instance).values_list('tag__slug', flat=True))
    tags_qs = Tag.objects.filter(slug__in=tag_slugs)
    return render(request, "postapp/post_edit.html", {
        'form': form,
        'instance': instance,
        'tags_qs': tags_qs,
        'active': 'post',
        'seo': get_settings(request.user.company_id),
    })


@login_required(login_url='/login/')
@staff_member_required
def post_view(request):
    """
    Галерея постов
    """
    message = None
    filter_dct = dict()
    slug_tag = request.GET.get('tag')

    if slug_tag:
        tag_obj = get_object_or_404(Tag, slug=slug_tag)
        post_ids = TagPost.objects.filter(tag=tag_obj).values_list('post_id', flat=True)
        filter_dct.update(
            {'id__in': post_ids}
        )
        message = f'Все посты по тегу "{tag_obj}"'

    slug = request.GET.get('charter')
    if slug:
        charter = Charter.objects.get(slug=slug)
        filter_dct.update(
            {'charter': charter}
        )
        message = f'Все посты в категории "{charter.title}"'

    date = request.GET.get('date')
    if date:
        filter_dct.update(
            {'date_post__startswith': date}
        )
        message = f'Все материалы за дату "{date}"'

    post_qs = Post.objects.for_user(request.user).filter(**filter_dct)
    query = request.GET.get('query')
    if query:
        query = query.strip()
        post_qs = post_qs.filter(
            Q(title__icontains=query) |
            Q(text__icontains=query) |
            Q(lead__icontains=query)
        )
        message = f'Все посты по поиску "{query}"'

    seo = get_settings(request.user.company_id)
    seo_title_lst = [seo["title"]]
    seo['seo_title'] = seo["title"]
    if message:
        seo_title_lst.append(message)

    page = int(request.GET.get('page')) if request.GET.get('page') else 1
    pagination_counter = 10
    if page > 1:
        post_qs = post_qs[(page-1)*pagination_counter:page*pagination_counter]
        keywords = Post.get_tags(post_qs, seo["keywords"])
        description_set = set(post_qs.values_list('title', flat=True))
        description_set.update({seo["description"]})
        description = list(description_set)
        date_lst = post_qs.values_list('date_post', flat=True)
        if date_lst:
            seo_title_lst.append(
                f"От {min(date_lst).strftime(f'%Y-%m-%d %H:%M:%S')} до {max(date_lst).strftime(f'%Y-%m-%d %H:%M:%S')}"
            )
    else:
        post_qs = post_qs[0:pagination_counter]
        keywords = Post.get_tags(post_qs, seo["keywords"])
        description_set = set(post_qs.values_list('title', flat=True))
        description_set.update({seo["description"]})
        description = list(description_set)

    seo['seo_title'] = ' | '.join(seo_title_lst)
    seo['seo_description'] = ', '.join(description)
    seo['seo_keywords'] = ', '.join(keywords)

    post_qs = Post.update_qs(post_qs)
    return render(request, "postapp/post_view.html", {
        'post_qs': post_qs,
        'active': 'post',
        'message': message,
        'seo': seo,
        'pagination': {
            'current': page,
            'counter': pagination_counter,
            'first': 1,
            'rw': page-1,
            'ff': page+1,
            'last': pagination_counter,
            'counter_lst': [x for x in range(page - 3, page + 4)],
        }
    })


def robots(request):
    robots = 'robots.txt'
    if request.environ['HTTP_HOST'] not in get_settings()['domain']:
        robots = 'disable.txt'
    return render(request, robots, content_type="text/plain")


@login_required(login_url='/login/')
@staff_member_required
def post_content(request, pk=None):
    """
    Проверка на плагиат
    """
    post = get_object_or_404(Post, pk=pk)
    text = post.text
    url = 'https://content-watch.ru/public/api/'

    data = {
        'action': 'CHECK_TEXT',
        'key': settings.API_KEY,
        'test': 0,
        'text': text
    }
    req = requests.post(url, data=data)
    req.encoding = 'utf-8'
    result_dct = req.json()
    post.title = f"{post.title}|{result_dct.get('percent')}"
    post.save()
    return redirect(news_detail, post.pk)


@login_required(login_url='/login/')
@staff_member_required
def charter_view(request):
    """
    Галерея категорий
    """
    message = None
    filter_dct = dict()
    charter_qs = Charter.objects.for_user(request.user).filter(**filter_dct)

    query = request.GET.get('query')
    if query:
        query = query.strip()
        charter_qs = charter_qs.filter(
            Q(title__icontains=query) |
            Q(lead__icontains=query)
        )
        message = f'Все категории по поиску "{query}"'
    user = request.user
    charter_dct = dict(Post.objects.for_user(user).order_by().values_list('charter_id').annotate(count=Count('id')))
    for charter in charter_qs:
        charter.count = charter_dct.get(charter.id, None)

    return render(request, "postapp/charter_view.html", {
        'charter_qs': charter_qs,
        'active': 'charter',
        'message': message,
        'seo': get_settings(request.user.company_id),
    })


@login_required(login_url='/login/')
@staff_member_required
def charter_edit(request, pk=None):
    instance = None
    if pk:
        instance = get_object_or_404(Charter, pk=pk)

    company = request.user.company
    init_dct = {
        'company_list': [(company.id, company.title)],
    }
    form = CharterForm(data=request.POST or None, files=request.FILES or None, instance=instance, init_dct=init_dct)
    if request.method == 'POST' and form.is_valid():
        cd = form.cleaned_data
        charter = form.save(commit=False)
        charter.slug = cyr_lat(cd['title'])
        charter.save()
        update_charter(request.user)
        return redirect(charter_view)

    return render(request, "postapp/charter_edit.html", {
        'form': form,
        'instance': instance,
        'active': 'charter',
        'seo': get_settings(request.user.company_id),
    })


def error404(request):
    template = loader.get_template('404.htm')
    context = Context({
        'message': 'All: %s' % request,
        })
    return HttpResponse(content=template.render(context), content_type='text/html; charset=utf-8', status=404)


@login_required(login_url='/login/')
@staff_member_required
def user_view(request):
    message = None
    user_qs = User.objects.for_user(request.user)
    query = request.GET.get('query')
    if query:
        query = query.strip()
        user_qs = user_qs.filter(
            Q(username__icontains=query) |
            Q(phone__icontains=query)
        )
        message = f'Все люди по поиску "{query}"'

    return render(request, "postapp/user_view.html", {
        'user_qs': user_qs,
        'active': 'user',
        'message': message,
        'seo': get_settings(request.user.company_id),
    })


@login_required(login_url='/login/')
@staff_member_required
def user_edit(request, pk=None):
    """
    Добавить или редактировать людей
    """
    instance = None
    if pk:
        instance = get_object_or_404(User, pk=pk)
    form = UserForm(data=request.POST or None, files=request.FILES or None, instance=instance)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect(user_view)

    return render(request, "postapp/user_edit.html", {
        'form': form,
        'instance': instance,
        'active': 'user',
        'seo': get_settings(request.user.company_id),
    })


@login_required(login_url='/login/')
@staff_member_required
def company_view(request):
    message = None
    company_qs = Company.objects.for_user(request.user)
    query = request.GET.get('query')
    if query:
        query = query.strip()
        company_qs = company_qs.filter(
            Q(title__icontains=query) |
            Q(slug__icontains=query)
        )
        message = f'Все компании по поиску "{query}"'

    return render(request, "postapp/company_view.html", {
        'company_qs': company_qs,
        'active': 'company',
        'message': message,
        'seo': get_settings(request.user.company_id),
    })


@login_required(login_url='/login/')
@staff_member_required
def company_edit(request, pk=None):
    """
    Добавить или отредактировать тег
    """
    instance = None
    if pk:
        instance = get_object_or_404(Company, pk=pk)
    form = CompanyForm(data=request.POST or None, files=request.FILES or None, instance=instance)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect(company_view)

    return render(request, "postapp/company_edit.html", {
        'form': form,
        'instance': instance,
        'active': 'company',
        'seo': get_settings(request.user.company_id),
    })


@login_required(login_url='/login/')
def user_password(request, pk=None):
    if not request.POST or not pk:
        raise Http404
    password = User.objects.make_random_password(length=random.randint(8, 12))
    user = get_object_or_404(User, pk=pk)
    user.set_password(password)
    user.save()
    messages.success(request, f'User `{user.email}` received a new password: {password}')
    return redirect('user_view')
