from django import forms

from advertapp.models import Advert


class AdvertForm(forms.ModelForm):
    """
    Форма редактирования рекламы
    """

    def __init__(self, *args, **kwargs):
        init_dct = kwargs.pop('init_dct', None)
        company_list = init_dct['company_list']
        super().__init__(*args, **kwargs)

        self.fields['company'].widget.attrs['class'] = 'select2'
        self.fields['company'].widget.choices = company_list

        self.fields['date_start'].widget.attrs.update({
            'class': 'form_datetime',
            'required': 'required',
            'data-provide': 'datetimepicker',
            'data-date-format': 'dd.mm.yyyy hh:ii:ss',
        })

        self.fields['date_stop'].widget.attrs.update({
            'class': 'form_datetime',
            'required': 'required',
            'data-provide': 'datetimepicker',
            'data-date-format': 'dd.mm.yyyy hh:ii:ss',
        })

    class Meta:
        model = Advert
        exclude = ['changed', 'created', 'deleted', 'counter_json']
