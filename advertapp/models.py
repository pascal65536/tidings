from datetime import datetime, timezone
import uuid
from django.db import models
from django.db.models import UUIDField, CharField, ForeignKey, ImageField, TextField, JSONField, DateTimeField

from advertapp.managers import AdvertManager
from newsapp.utils import latin_filename
from people.models import Company


class Advert(models.Model):
    """
    Реклама
    """

    CHOICE_POSITION = (
        ('top', 'Сверху'),
        ('bottom', 'Снизу'),
        ('content', 'Контент'),
        ('skyscraper', 'Небоскреб'),
    )
    CHOICE_COLOR = (
        ('primary', 'primary'),
        ('secondary', 'secondary'),
        ('success', 'success'),
        ('danger', 'danger'),
        ('warning', 'warning'),
        ('info', 'info'),
        ('light', 'light'),
        ('dark', 'dark'),
        ('white', 'white'),
        ('transparent', 'transparent'),
    )

    slug = UUIDField(verbose_name='Публичный код', default=uuid.uuid4, db_index=True, unique=True, editable=False)
    title = CharField(verbose_name='Название', max_length=255)
    company = ForeignKey(Company, related_name='advert_company',
                         blank=True, null=True, verbose_name='Компания', on_delete=models.SET_NULL)
    image = ImageField(verbose_name='Картинка', upload_to=latin_filename, blank=True, null=True)
    color = CharField(verbose_name=u'Заливка', choices=CHOICE_COLOR, max_length=255)
    url = CharField(verbose_name=u'Ссылка', max_length=255, blank=True, null=True)
    html = TextField(verbose_name='Код', blank=True, null=True)
    position = CharField(verbose_name='Положение', choices=CHOICE_POSITION, max_length=255)
    counter_json = JSONField(verbose_name='Клики по рекламе', default=dict)
    date_start = DateTimeField(verbose_name='Дата начала')
    date_stop = DateTimeField(verbose_name='Дата конца')
    created = DateTimeField(verbose_name='Created', auto_now_add=True)
    changed = DateTimeField(verbose_name='Changed', auto_now=True)
    deleted = DateTimeField(verbose_name='Deleted', blank=True, null=True, help_text='Set datetime, when it was deleted')

    objects = AdvertManager()

    @property
    def is_present(self):
        """
        Agreement is acting now?
        :return:
        """
        today = datetime.now(timezone.utc)
        return self.date_start < today < self.date_stop

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Реклама'
        verbose_name_plural = 'Реклама'
        ordering = ['-date_start']
