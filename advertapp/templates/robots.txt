User-agent: *
Allow: /

User-agent: Yandex
Allow: /
Allow: /list
Allow: /detail

Disallow: /advert
Disallow: /tags
Disallow: /admin
Disallow: /charter
Disallow: /post
Disallow: /photo

Sitemap: http://krasnoarsk.ru/sitemap.xml

Host: http://krasnoarsk.ru/
