from django.contrib import admin
from advertapp.models import Advert


@admin.register(Advert)
class AdvertAdmin(admin.ModelAdmin):
    list_display = ('title', 'company', 'color', )
    list_filter = ('changed', )
    fieldsets = (
        (None, {'fields': ('title', 'image',)}),
    )
