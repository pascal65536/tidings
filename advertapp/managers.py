from django.db import models
from django.utils import timezone
from datetime import datetime, timezone

from people.models import User


class AdvertManager(models.Manager):
    """
    Менеджер рекламы.
    """
    def for_show(self):
        today = datetime.now(timezone.utc)
        qs = self.filter(date_start__lte=today, date_stop__gte=today, deleted__isnull=True)
        return qs

    def for_user(self, user, company=None):

        if isinstance(user, User) and user.is_superuser:
            return self.all()

        if isinstance(user, User) and user.is_staff:
            return self.filter(company=user.company)

        if company:
            return self.filter(company=company)
        return self.none()
