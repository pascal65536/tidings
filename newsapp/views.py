from django.db.models import Q
from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone

from newsapp.utils import get_recent_for_tags, find_img, get_num, get_settings
from people.models import Company
from postapp.models import Charter, Post, Tag, TagPost
from django.contrib.sitemaps import Sitemap
import datetime


def calendar(request):
    year = request.GET.get('year')
    if not year:
        year = timezone.now().year
    year = int(year)

    company = Company.objects.filter(domain__contains=request.environ['HTTP_HOST'])[0]
    year_lst = set()
    post_qs = Post.objects.for_user(user=request.user, company=company).order_by('date_post')
    for post in range(post_qs.first().date_post.year, post_qs.last().date_post.year + 1):
        year_lst.add(post)
    year_lst = list(year_lst)
    sorted(year_lst)

    post_qs = Post.objects.for_user(user=request.user, company=company).filter(date_post__startswith=year).order_by('date_post')
    post_dct = dict()
    for m in range(1, 13):
        first_day = datetime.date(year, m, 1)
        length_mons = (first_day - datetime.timedelta(days=1))
        post_dct.setdefault(length_mons.month, {}).setdefault('length', length_mons.day)
        post_dct.setdefault(m, {}).setdefault('name', first_day.strftime("%B"))
        post_dct.setdefault(m, {}).setdefault('weekday', first_day.isoweekday())
        post_dct.setdefault(m, {}).setdefault('date', [])

    for m in range(1, 13):
        count = 0
        show = ''
        cl = list()
        ls = list()
        for d in range(1, 43):
            if len(ls) == 7:
                cl.append(ls)
                ls = list()
            if d >= post_dct[m]['weekday']:
                count += 1
                show = count
            if count > post_dct[m]['length']:
                show = ''
            ls.append(show)
        cl.append(ls)
        post_dct.setdefault(m, {}).setdefault('calendar', cl)

    for post in post_qs:
        post_dct[post.date_post.month]['date'].append(post.date_post.day)

    seo = get_settings(request.user.company_id)
    return render(request, "newsapp/calendar.html", {
        'post_dct': dict(sorted(post_dct.items(), key=lambda item: item[0])),
        'year': year,
        'year_lst': year_lst,
        'seo': seo
    })


def news_view(request):
    """
    Галерея новостей
    """
    message = None
    filter_dct = dict()
    slug_tag = request.GET.get('tag')
    company = Company.objects.filter(domain__contains=request.environ['HTTP_HOST'])[0]
    post_qs = Post.objects.for_user(request.user, company=company).order_by('-date_post')

    if slug_tag:
        tag_obj = get_object_or_404(Tag, slug=slug_tag)
        post_ids = TagPost.objects.filter(tag=tag_obj).values_list('post_id', flat=True)
        filter_dct.update(
            {'id__in': post_ids}
        )
        message = f'Публикации по тегу "{tag_obj}"'

    slug = request.GET.get('charter')
    if slug:
        charter = get_object_or_404(Charter, slug=slug)
        filter_dct.update(
            {'charter': charter}
        )
        message = f'Публикации в категории "{charter.title}"'

    date = request.GET.get('date')
    if date:
        filter_dct.update(
            {'date_post__startswith': date}
        )
        message = f'Публикации за дату "{date}"'

    query = request.GET.get('query')
    if query:
        query = query.strip()
        post_qs = post_qs.filter(
            Q(title__icontains=query) |
            Q(text__icontains=query) |
            Q(lead__icontains=query)
        )
        message = f'Публикации по поиску "{query}"'

    post_qs = post_qs.filter(**filter_dct)

    seo = get_settings()
    seo_title_lst = [seo["title"]]
    seo['seo_title'] = seo["title"]
    if message:
        seo_title_lst.append(message)

    page = int(request.GET.get('page')) if request.GET.get('page') else 1
    pagination_counter = 18
    if page > 1:
        post_qs = post_qs[(page-1)*pagination_counter:page*pagination_counter]
        keywords = Post.get_tags(post_qs, seo["keywords"])
        description_set = set(post_qs.values_list('title', flat=True))
        description_set.update({seo["description"]})
        description = list(description_set)

        date_lst = post_qs.values_list('date_post', flat=True)
        if date_lst:
            seo_title_lst.append(
                f"От {min(date_lst).strftime(f'%Y-%m-%d %H:%M:%S')} до {max(date_lst).strftime(f'%Y-%m-%d %H:%M:%S')}"
            )
    else:
        post_qs = post_qs[0:pagination_counter]
        keywords = Post.get_tags(post_qs, seo["keywords"])
        description_set = set(post_qs.values_list('title', flat=True))
        description_set.update({seo["description"]})
        description = list(description_set)

    seo['seo_title'] = ' | '.join(seo_title_lst)
    seo['seo_description'] = ', '.join(description)
    seo['seo_keywords'] = ', '.join(keywords)

    post_qs = Post.update_qs(post_qs)

    main_qs = post_qs[0:6]
    page_qs = post_qs[6:]

    post_idx = [pp.id for pp in post_qs]
    recent_qs = Post.objects.for_user(request.user, company=company).exclude(id__in=post_idx).order_by('-date_post')[0:12]

    return render(request, "newsapp/news_view.html", {
        'main_qs': main_qs,
        'recent_qs': recent_qs,
        'page_qs': page_qs,
        'message': message,
        'seo': seo,
        'pagination': {
            'current': page,
            'counter': pagination_counter,
            'first': 1,
            'rw': page-1,
            'ff': page+1,
            'last': pagination_counter,
            'counter_lst': [x for x in range(page - 3, page + 4)],
        },
    })


def news_short_list(request):
    recent_qs = Post.objects.for_user(request.user).order_by('id')
    return render(request, "newsapp/news_short_list.html", {
        'recent_qs': recent_qs,
        'seo': get_settings(request.user.company_id),
    })


def news_short(request, short=None):
    """
    Распакуем короткую ссылку
    """
    if not short:
        return redirect('news_view')
    pk = get_num(short)
    post = get_object_or_404(Post, pk=pk)
    if not post.get_short() == short:
        raise Http404
    return redirect('news_detail', pk=post.pk)


def news_detail(request, pk=None):
    """
    Детали поста
    """
    company = None
    if request.environ['HTTP_HOST']:
        company = Company.objects.filter(domain__contains=request.environ['HTTP_HOST'])[0]
    post_qs = Post.objects.for_user(request.user, company=company)
    try:
        instance_qs = post_qs.filter(pk=pk)
        instance_qs = Post.update_qs(instance_qs)
        if len(instance_qs) != 1:
            raise Http404
        instance = instance_qs[0]
    except Charter.DoesNotExist:
        raise Http404
    instance.text = find_img(instance.text)

    tags_ids = set(TagPost.objects.filter(post_id=instance.pk).values_list('tag_id', flat=True))
    tags_qs = Tag.objects.filter(id__in=tags_ids)
    recent_post_idx = get_recent_for_tags(instance, request.user)
    recent_post_qs = Post.objects.filter(id__in=recent_post_idx[0:16])
    if recent_post_qs.count() == 0:
        recent_post_qs = Post.objects.all()[0:16]

    return render(request, "newsapp/news_detail.html", {
        'instance': instance,
        'tags_qs': tags_qs,
        'active': instance.charter.slug,
        'recent_post_qs': recent_post_qs,
        'seo': get_settings(company.id),
    })


class PostSitemap(Sitemap):
    changefreq = "never"
    priority = 0.5

    def items(self):
        return Post.objects.filter(deleted__isnull=True, date_post__lte=timezone.now())

    def lastmod(self, obj):
        return obj.date_post
