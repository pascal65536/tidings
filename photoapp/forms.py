from django import forms
from photoapp.models import Photo


class PhotoForm(forms.ModelForm):
    """
    Форма для редактирования фото
    """
    def __init__(self, *args, **kwargs):
        init_dct = kwargs.pop('init_dct', None)
        company_list = init_dct['company_list']
        super().__init__(*args, **kwargs)

        self.fields['company'].widget.attrs['class'] = 'select2'
        self.fields['company'].widget.choices = company_list

        self.fields['deleted'].widget.attrs.update({
            'class': 'form_datetime',
            'data-provide': 'datetimepicker',
            'data-date-format': 'dd.mm.yyyy hh:ii:ss',
        })

    class Meta:
        model = Photo
        exclude = ['created', 'changed']
