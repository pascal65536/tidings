from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.core.mail import message
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect

from newsapp.utils import get_settings
from people.models import Company
from photoapp.forms import PhotoForm
from photoapp.models import Photo
from django.shortcuts import render
from django.template import RequestContext

from postapp.models import Tag


@login_required(login_url='/login/')
@staff_member_required
def photo_edit(request, pk=None):
    """
    Добавить или редактировать фото
    """
    instance = None
    if pk:
        instance = get_object_or_404(Photo, pk=pk)

    company = request.user.company
    init_dct = {
        'company_list': [(company.id, company.title)],
    }
    form = PhotoForm(data=request.POST or None,
                     files=request.FILES or None,
                     instance=instance,
                     init_dct=init_dct)
    if request.method == 'POST' and form.is_valid():
        form.save()
        return redirect(photo_view)

    return render(request, "photoapp/photo_edit.html", {
        'form': form,
        'instance': instance,
        'active': 'photo',
    })


@login_required(login_url='/login/')
@staff_member_required
def photo_view(request):
    """
    Фотогалерея
    :param request:
    :return:
    """
    message = None
    filter_dct = dict()
    slug_tag = request.GET.get('tag')
    if slug_tag:
        filter_dct.update(
            {'tags__slug': slug_tag}
        )
        tag_obj = get_object_or_404(Tag, slug=slug_tag)
        message = f'Все фотографии по тегу "{tag_obj}"'

    company = Company.objects.filter(domain__contains=request.environ['HTTP_HOST'])[0]
    photo_qs = Photo.objects.for_user(request.user, company=company).filter(**filter_dct).order_by('deleted', '-changed')
    query = request.GET.get('query')
    if query:
        query = query.strip()
        photo_qs = photo_qs.filter(
            Q(title__icontains=query) |
            Q(description__icontains=query)
        )
        message = f'Все фотографии по поиску "{query}"'

    seo = get_settings()
    page = int(request.GET.get('page')) if request.GET.get('page') else 1
    pagination_counter = 16
    if page:
        photo_qs = photo_qs[(page-1)*pagination_counter:page*pagination_counter]
    else:
        photo_qs = photo_qs[0:pagination_counter]

    return render(request, "photoapp/photo_view.html", {
        'photo_qs': photo_qs,
        'active': 'photo',
        'message': message,
        'seo': seo,
        'pagination': {
            'current': page,
            'counter': pagination_counter,
            'first': 1,
            'rw': page - 1,
            'ff': page + 1,
            'last': pagination_counter,
            'counter_lst': [x for x in range(page - 3, page + 4)],
        }
    })


def e_handler404(request):
    context = RequestContext(request)
    response = render('404.html', context)
    response.status_code = 404
    return response


def e_handler500(request):
    context = RequestContext(request)
    response = render('500.html', context)
    response.status_code = 500
    return response
