from django.db import models
from people.models import User


class PhotoManager(models.Manager):
    """
    Менеджер фоток.
    """
    def for_user(self, user, company=None):

        if isinstance(user, User) and user.is_superuser:
            return self.filter(company=user.company)

        if isinstance(user, User) and user.is_staff:
            return self.filter(company=user.company)

        if company:
            return self.filter(deleted__isnull=True, company=user.company)
        return self.none()
