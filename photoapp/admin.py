from django.contrib import admin
from photoapp.models import Photo


@admin.register(Photo)
class PhotoAdmin(admin.ModelAdmin):
    list_display = ('title', 'company', 'image_img')
    list_filter = ('changed', )
    fieldsets = (
        (None, {'fields': ('title', 'picture', 'company', 'description', )}),
    )
